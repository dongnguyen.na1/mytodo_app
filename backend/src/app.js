const express = require('express');
const getAllroute = require('express-list-endpoints');

const router = require('./router');

const app = new express();
const port = 3000;
app.use('/api', router);
app.listen(port, () => {
    console.log('TODO app listening on port ${port}!');
    console.log('Registered Routes: ');
    console.log(getAllroute(app));
});