const express = require('express');
const router = express.Router();
const { createTodo, updateTodo, deleteTodo, getTodoList, clearCompleted } = require('../controller/TodoController');

router.post('/api/createTodo', createTodo);

router.post('/api/getTodos', getTodoList);
router.post('/api/deleteTodo', deleteTodo);
router.post('/api/clearcompleted', clearCompleted);
router.post('/api/updateTodo', updateTodo);

module.exports = router;