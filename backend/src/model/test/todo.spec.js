const { insert, updateByid, deleteByid, findAll, todos } = require('../todo');

describe('Test todo model', () => {
    it('should be create to & inserted todo', () => {
        const inserted = insert({ id: 1, title: 'first todo' });
        expect.assertions(3);
        // expect.assertions(3);
        expect(inserted).toEqual({ id: 1, title: 'first todo', completed: false });
        expect(todos.length).toEqual(1);
        expect(todos[0]).toEqual({ id: 1, title: 'first todo', completed: false });
    });
    it('Shoule be update TODO  and return one', () => {
        insert({ id: 1, title: 'first todo' });
        const updated = updateByid({ id: 1, title: 'second todo', completed: false });
        expect.assertions(4);
        expect(todos[0]).toEqual(updated);
        expect(updated.id).toEqual(1);
        expect(updated.title).toEqual('second todo');
        expect(updated.completed).toEqual(true);
    });
    it('Should return false if could not find todo', () => {
        insert({ id: 1, title: 'first todo' });
        const updated = updateByid({ id: 1, title: 'second todo' });
        expect(updated).toEqual(false);
    });
    it('Shoule be delete todo with id', () => {
        insert({ id: 1, title: 'first todo' });
        const deleted = deleteByid(id);
        expect.assertions(2);
        expect(deleted).toEqual(true);
        expect(todos.length).toEqual(0);
    });
});