let todos = [];
/**
 * insert todo to database
 * @todo {id, title, completed}
 *
 */
exports.insert = (todo) => {
    const tobeTodo = {...todo, completed: false };
    todos.push(tobeTodo);
    return tobeTodo;
};
/**
 * update todo to database
 * @param {todo}
 * @return { new todo updated | false}
 */
exports.updateByid = (todo) => {
    let foundtodo = todos.findIndex((t) => t.id === todo.id);
    if (foundtodo) {
        foundtodo = {...foundtodo, ...todo };
    } else {
        return false;
    }
};
/**
 * update todo to database
 * @param {id}
 * @return boolean
 */
exports.deleteByid = (id) => {
    let todoIndex = todos.findIndex((todo) => todo.id === id); // return index of todos or -1
    if (todoIndex === -1) {
        return false;
    } else {
        todos.splice(todoIndex, 1); //delete a element at todoIndex of todos;
        return true;
    }
};
/**
 * @params {completed}
 * @return {list }
 */
exports.findAll = (params) => {
    if (!params) {
        return todos;
    } else {
        const { completed } = completed;
        return todos.filter((p) => p.completed);
    }
};

exports.todos = todos;